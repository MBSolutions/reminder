#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Reminder Configuration'
    _name = 'reminder.configuration'
    _description = __doc__

    reminder_from_email_prefix = fields.Char('Reminder From-eMail Prefix')
    reminder_email_template = fields.Property(
        fields.Many2One('reminder.email.template',
        'Reminder Email Template'))

Configuration()
